# prepare deps
apt update
apt install bzip2 git vim make gcc libncurses-dev flex bison bc cpio libelf-dev libssl-dev

# build kernel
git clone --depth 1 https://github.com/torvalds/linux.git
cp linux.config linux/.config
cd linux && make

# move kernel binary to /boot-files
mkdir /boot-files
mv arch/x86/boot/bzImage /boot-files

# build busybox
cd ..
git clone --depth 1 https://git.busybox.net/busybox
cp busybox.config busybox/.config
cd busybox && make

# setup init filesystem
mkdir /boot-files/initramfs
make CONFIG_PREFIX=/boot-files/initramfs install
rm /boot-files/initramfs/linuxrc

# create initramfs init
cat > /boot-files/initramfs/init <<EOF
#!/bin/sh
/bin/sh
EOF
chmod +x /boot-files/initramfs/init

# create init.cpio
cd /boot-files/initramfs
find . | cpio -o -H newc > /boot-files/init.cpio

# set up bootloader
cd ..
apt install syslinux
dd if=/dev/zero of=boot bs=1M count=50
apt install dosfstools
mkfs -t fat boot
syslinux boot

# copy kernel to boot
mkdir m
mount boot m
cp bzImage init.cpio m
unmount m
